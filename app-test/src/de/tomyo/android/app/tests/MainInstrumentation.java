package de.tomyo.android.app.tests;

import android.test.ActivityInstrumentationTestCase2;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import de.tomyo.android.app.Main;
import de.tomyo.android.app.R;
import junit.framework.Assert;

/**
 * This is a simple framework for a test of an Application.  See
 * {@link android.test.ApplicationTestCase ApplicationTestCase} for more information on
 * how to write and extend Application tests.
 * <p/>
 * To run this test, you can type:
 * adb shell am instrument -w \
 * -e class de.tomyo.android.mainTest \
 * de.tomyo.android.tests/android.test.InstrumentationTestRunner
 */
public class MainInstrumentation extends ActivityInstrumentationTestCase2<Main> {

    Main activity;
    EditText input;
    TextView output;
    Button okButton;

    public MainInstrumentation() {
        super(Main.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();

        setActivityInitialTouchMode(false);

        activity = getActivity();
        input = (EditText) activity.findViewById(R.id.main_textf_simple_input);
        output = (TextView) activity.findViewById(R.id.main_text_simple);
        okButton = (Button) activity.findViewById(R.id.main_btn_ok);
    }

    public void testPreConditions() {
        Assert.assertTrue(activity != null);
        Assert.assertTrue(input != null);
        Assert.assertTrue(output != null);
        Assert.assertTrue(okButton != null);
    }

    public void testUiMainFunction()  {
        activity.runOnUiThread(
            new Runnable() {

                @Override
                public void run() {
                    input.requestFocus();
                    input.setText("abc");
                    okButton.requestFocus();
                    okButton.callOnClick();

                    Assert.assertEquals("cba", output.getText().toString());
                }

            }
        );
    }

//    public void testFail() {
//        Assert.assertTrue(false);
//    }

}
