package de.tomyo.android.app;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import de.tomyo.android.applib.TextUtils;

public class Main extends Activity implements OnClickListener {
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		registerUiListerner();
	}

	private void registerUiListerner() {
		 ((Button) findViewById(R.id.main_btn_ok)).setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		final int id = v.getId();
		switch (id) {
		case R.id.main_btn_ok:
			syncIOText();
			break;
		default:
			//nothing...	
		}
	}

	private void syncIOText() {
		final EditText input = ((EditText) findViewById(R.id.main_textf_simple_input));
		final TextView output = ((TextView) findViewById(R.id.main_text_simple));
		
		final String reversedText = TextUtils.reverse(input.getText().toString());
		output.setText(reversedText);
	}

}
