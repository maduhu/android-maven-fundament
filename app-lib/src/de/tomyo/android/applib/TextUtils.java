package de.tomyo.android.applib;

/**
 * Simple util-class to test the integration of the app-lib.
 * The app-lib also includes the common-lang StringUtils to the the integration of
 * external dependenices in lib-projects.
 * 
 * @author thomas.onken
 */
public final class TextUtils {

	private TextUtils(){
		// Util-Class
	}
	
	public static String reverse(String str) {
		if (str == null) {
			return null;
		}
		final StringBuilder buffer = new StringBuilder(str.length());
		for (int i = str.length() - 1; i >= 0; i--) {
			buffer.append(str.charAt(i));
		}
		return buffer.toString();
	}
}
